from django.apps import AppConfig


class AppteveoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'AppTeveo'
