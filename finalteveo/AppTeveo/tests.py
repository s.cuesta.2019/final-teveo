from django.test import TestCase, Client
from django.urls import reverse
from .models import  Camara

class TestAppTeveoEndToEnd(TestCase):

    def setUp(self):
        self.client = Client()
        # Agregar datos de prueba, como cámaras, comentarios y usuarios, si es necesario

    def test_index_page(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'AppTeveo/pagina_principal.html')
        # Agregar más aserciones para verificar la presencia de elementos específicos en la página

    def test_help_page(self):
        response = self.client.get(reverse('ayuda'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'AppTeveo/pagina_ayuda.html')
        # Agregar más aserciones para verificar la presencia de elementos específicos en la página

    def test_camara_page(self):
        # Crear una cámara de prueba
        camara = Camara.objects.create(nombre="Cámara de prueba", url="http://example.com", lugar="Ubicación de prueba", latitud=0, longitud=0)
        response = self.client.get(reverse('camara', args=[camara.id]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'AppTeveo/pagina_camara.html')
        # Agregar más aserciones para verificar la presencia de elementos específicos en la página

    def test_camaras_page(self):
        response = self.client.get(reverse('camaras'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'AppTeveo/pagina_camaras.html')
        # Agregar más aserciones para verificar la presencia de elementos específicos en la página

    def test_camara_dinamica_page(self):
        # Crear una cámara de prueba
        camara = Camara.objects.create(nombre="Cámara de prueba", url="http://example.com", lugar="Ubicación de prueba", latitud=0, longitud=0)
        response = self.client.get(reverse('camara_dinamica', args=[camara.id]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'AppTeveo/pagina_camara_dinamica.html')
        # Agregar más aserciones para verificar la presencia de elementos específicos en la página

    def test_configuracion_page(self):
        response = self.client.get(reverse('configuracion'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'AppTeveo/pagina_configuracion.html')
        # Agregar más aserciones para verificar la presencia de elementos específicos en la página

    def test_comentarios_page(self):
        # Crear una cámara de prueba
        camara = Camara.objects.create(nombre="Cámara de prueba", url="http://example.com", lugar="Ubicación de prueba", latitud=0, longitud=0)
        response = self.client.get(reverse('comentarios', args=[camara.id]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'AppTeveo/pagina_comentario.html')
        # Agregar más aserciones para verificar la presencia de elementos específicos en la página

    def test_camara_json(self):
        # Crear una cámara de prueba
        camara = Camara.objects.create(nombre="Cámara de prueba", url="http://example.com", lugar="Ubicación de prueba", latitud=0, longitud=0)
        response = self.client.get(reverse('camara_json', args=[camara.id]))
        self.assertEqual(response.status_code, 200)
        # Agregar más aserciones para verificar la estructura y contenido del JSON devuelto
