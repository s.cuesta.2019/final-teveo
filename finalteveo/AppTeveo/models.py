from django.db import models
import random
import string

# Create your models here.

class Camara(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    url = models.URLField()
    lugar = models.CharField(max_length=250)
    latitud = models.FloatField()
    longitud = models.FloatField()

class Comentario(models.Model):
    nombre = models.CharField(max_length=100)
    camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    contenido = models.TextField()
    fecha = models. DateTimeField(auto_now_add=True)
    img_src = models.CharField(max_length=200)


class Usuario(models.Model):
    userName = models.CharField(max_length=100, default='Anónimo')
    sizeSelect = models.CharField(max_length=10)
    fontSelect = models.CharField(max_length=50)
    sesion_id = models.CharField(max_length=200,default='abcdefghyjklmñopkrst')


class Like(models.Model):
    Camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    Usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    
    def __str__(self):
        return f"Like del usuario {self.Usuario.userName} a la cámara {self.Camara.nombre}"
