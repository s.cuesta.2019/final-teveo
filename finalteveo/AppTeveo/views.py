import os
import datetime
import base64
import requests
import random
import string
from .models import Comentario, Camara, Usuario, Like
from django.shortcuts import render,redirect, get_object_or_404
from django.core.paginator import Paginator
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
import xml.etree.ElementTree as ET
from django.conf import settings
from django.db.models import Count
from django.urls import reverse


def contador():
    #cuenta las camaras y los comentarios que hay
    numero_camaras = Camara.objects.count()
    numero_comentarios = Comentario.objects.count()
    return numero_camaras, numero_comentarios


def pagina_ayuda(request):

    session_key = request.session.session_key
    configUsuario = Usuario.objects.filter(sesion_id=session_key).first()
    if not configUsuario:
        configUsuario = configUsuario(userName='Anónimo')

    numero_camaras, numero_comentarios = contador()

    context = {
        'numero_camaras' : numero_camaras,
        'numero_comentarios' : numero_comentarios,
        'configUsuario' : configUsuario,
    }
    return render(request, 'AppTeveo/pagina_ayuda.html', context)


def index(request):

    # Crear una sesión si no existe
    if not request.session.session_key:
        request.session.create()

    # Verificar si ya existe un usuario anónimo  en la base de datos
    usuario_anonimo = Usuario.objects.filter(userName='Anónimo').first()
    if not usuario_anonimo:
        usuario_anonimo= Usuario.objects.create(
            userName='Anónimo',
            sesion_id= 'abcdefghyjklmñopkrst',
            sizeSelect='medium',
            fontSelect='Arial'
        )

    # Obtener la clave de sesión actual
    session_key = request.session.session_key

    # Verificar si ya existe un usuario con la sesión actual
    configUsuario = Usuario.objects.filter(sesion_id=session_key).first()
    if not configUsuario:
        configUsuario = Usuario.objects.create(
            userName='Anónimo',
            sesion_id=session_key,
            sizeSelect='medium',
            fontSelect='Arial'
        )

    comentarios = Comentario.objects.all().order_by('-fecha')

    paginator = Paginator(comentarios, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    numero_camaras, numero_comentarios = contador()

    context = {
        'comentarios': page_obj,
        'numero_camaras': numero_camaras,
        'numero_comentarios': numero_comentarios,
        'page_obj': page_obj,
        'configUsuario' : configUsuario,
    }
    return render(request, 'AppTeveo/pagina_principal.html', context)


def camaras(request):

    session_key = request.session.session_key
    configUsuario = Usuario.objects.filter(sesion_id=session_key).first()
    if not configUsuario:
        configUsuario = configUsuario(userName='Anónimo')

    camaras = Camara.objects.annotate(num_comentarios=Count('comentario')).order_by('-num_comentarios')
    #obtiene todas las cámaras de la base de datos, junto con el número de comentarios asociados y los ordena

    if not camaras:  # Si no hay cámaras disponibles
        return render(request, 'AppTeveo/pagina_camaras.html')
    
    # elegir aleatoriamente una cámara para mostrar como cámara aleatoria en la página
    random_cam = random.choice(camaras).url

    numero_camaras, numero_comentarios = contador()

    context = {
        'camaras': camaras,
        'random_cam': random_cam,
        'numero_camaras': numero_camaras,
        'numero_comentarios': numero_comentarios,
        'configUsuario' : configUsuario,
    }
    return render(request, 'AppTeveo/pagina_camaras.html', context)


def camara(request, id):

    session_key = request.session.session_key
    configUsuario = Usuario.objects.filter(sesion_id=session_key).first()
    if not configUsuario:
        configUsuario = configUsuario(userName='Anónimo')

    es_dinamica = id.endswith("-dyn")
    id_camara = id[:-4] if es_dinamica else id
    
    camara = get_object_or_404(Camara, id=id_camara)
    comentarios = Comentario.objects.filter(camara=camara).order_by("-fecha")
    
    # Obtener otros datos necesarios para el contexto
    numero_camaras, numero_comentarios = contador()

    # Obtener el número de likes de la cámara actual
    num_likes = Like.objects.filter(Camara=camara).count()

    if request.method == 'POST':
            if Usuario.userName == "Anónimo":
                return redirect('camara', id=id)  # Otra acción que desees tomar si el usuario es anónimo
            # Manejar la solicitud POST para registrar un "like" si no existe ya una combinación de cámara y usuario
            # Verificar si el usuario ya ha dado like
            if not Like.objects.filter(Camara=camara, Usuario=configUsuario).exists():
                like = Like(Camara=camara, Usuario=configUsuario)
                like.save()
                # Redirigir de nuevo a la página de perfil de la cámara después de registrar el "like"
                return redirect('camara', id=id)

    
    # Actualizar el contexto con el número de likes y otros datos
    context = {
        'numero_camaras': numero_camaras,
        'numero_comentarios': numero_comentarios,
        'camara': camara,
        'comentarios': comentarios,
        'configUsuario' : configUsuario,
        'num_likes' : num_likes,
    }
    
    return render(request, "AppTeveo/pagina_camara.html", context)


def camara_dinamica(request, id):

    session_key = request.session.session_key
    configUsuario = Usuario.objects.filter(sesion_id=session_key).first()
    if not configUsuario:
        configUsuario = configUsuario(userName='Anónimo')

    #obtener camara
    camara = get_object_or_404(Camara, id=id)
    
    #obtener comentarios para la pagina
    comentarios = Comentario.objects.filter(camara=camara).order_by("-fecha")
    numero_camaras, numero_comentarios = contador()

    context = {
        'numero_camaras' : numero_camaras,
        'numero_comentarios' : numero_comentarios,
        'camara': camara,
        'comentarios': comentarios,
        'configUsuario' : configUsuario,
    } 
    return render(request, "AppTeveo/pagina_camara_dinamica.html", context)


def imagen_dinamica(request, id):
    
    camara = get_object_or_404(Camara, id=id)
    url_imagen = camara.url 

    try:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'
        }
        #diccionario de encabezados se utiliza para proporcionar información adicional sobre la solicitud HTTP
        response = requests.get(url_imagen, headers=headers)
        response.raise_for_status()  

        imagen_base64 = base64.b64encode(response.content).decode('utf-8')
        #Codifica el contenido de la respuesta en formato base64
        contenido_html = f'<img src="data:image/jpeg;base64,{imagen_base64}" style="width: 50%; height: auto;">'
        # Construye una cadena HTML que contiene una etiqueta <img> que muestra la imagen incrustada
        return HttpResponse(contenido_html)
    except Exception as e:
        return HttpResponse("Error al cargar la imagen", status=500)
    

def comentarios_dinamicos(request, id):
    #separado de comentarios para que se puedan actualizar solo los campos y no la pagina entera
    camara = get_object_or_404(Camara, id=id)
    comentarios = Comentario.objects.filter(camara=camara).order_by("-fecha")
    return render(request, 'AppTeveo/comentarios_dinamicos.html', {'comentarios': comentarios})


def configuracion(request):
    session_key = request.session.session_key
    configuracion = Usuario.objects.filter(sesion_id=session_key).first()
    if not configuracion:
        configuracion = Usuario(userName='Anónimo')  # Corrección aquí para crear una nueva instancia

    numero_camaras, numero_comentarios = contador()

    if request.method == 'POST':
        if 'logout' in request.POST:
            if configuracion.userName == "Anónimo":
                return redirect('/teveo/')
            else:
                configuracion.delete()
                return redirect('/teveo/')

        # Actualizar manualmente los campos sin usar UsersetupForm
        configuracion.userName = request.POST.get('userName', configuracion.userName)
        configuracion.sizeSelect = request.POST.get('sizeSelect', configuracion.sizeSelect)
        configuracion.fontSelect = request.POST.get('fontSelect', configuracion.fontSelect)
        
        configuracion.sesion_id = session_key
        configuracion.save()

    # Verificar si se ha solicitado un enlace de autorización
        if 'authorize_link' in request.POST:
            # Generar un identificador único para la sesión
            authorize_url = f"{request.build_absolute_uri()}?{session_key}"
            context = {
                'numero_camaras': numero_camaras,
                'numero_comentarios': numero_comentarios,
                'configUsuario': configuracion,
                'authorize_url': authorize_url
            }
            return render(request, 'AppTeveo/pagina_configuracion.html', context)
        else:
            return redirect('/teveo/')

    else:
        # Verificar si se ha proporcionado una URL de sesión
        session_url = request.GET.get('session_url')
        if session_url:
            try:
                # Extraer la ID de sesión de la URL
                session_id = session_url.split('?')[-1]
                # Buscar la configuración con la ID de sesión
                authorized_settings = Usuario.objects.get(sesion_id=session_id)
                # Copiar la configuración a la sesión actual
                configuracion.userName = authorized_settings.userName
                configuracion.sizeSelect = authorized_settings.sizeSelect
                configuracion.fontSelect = authorized_settings.fontSelect
                configuracion.save()
                return redirect('/teveo/')
            except (Usuario.DoesNotExist, ValueError):
                context['error_message'] = "No existe esa ID de la sesión"

    context = {
        'numero_camaras': numero_camaras,
        'numero_comentarios': numero_comentarios,
        'configUsuario': configuracion,
    }
    return render(request, 'AppTeveo/pagina_configuracion.html', context)



def comentarios(request, id):

    session_key = request.session.session_key
    configUsuario = Usuario.objects.filter(sesion_id=session_key).first()
    if not configUsuario:
        configUsuario = configUsuario(userName='Anónimo')

    # Obtener el objeto Camera usando get_object_or_404 para manejar automáticamente el error HTTP 404
    camara = get_object_or_404(Camara, id=id)
    
    # Procesar el POST request para guardar un nuevo comentario
    if request.method == 'POST':
        # Obtén el comentario del formulario y crea un nuevo objeto Comment
        comment_text = request.POST.get('comment', '').strip()
        if comment_text:  # Asegúrate que el comentario no esté vacío
            new_comment = Comentario(camara_id=camara.id, contenido=comment_text, fecha=datetime.datetime.now())
            new_comment.save()
            # Redirecciona a la misma página para evitar duplicación en caso de que el usuario actualice la página
            return redirect(reverse("camara", args=[id]))

    # Obtener todos los comentarios ordenados por fecha de forma descendente
    commentarios = Comentario.objects.filter(camara_id=camara.id).order_by('-fecha')

    numero_camaras, numero_comentarios = contador()

    context = {
        'numero_camaras' : numero_camaras,
        'numero_comentarios' : numero_comentarios,
        'camara': camara,
        'commentarios': commentarios,
        'configUsuario' : configUsuario,
    }
    return render(request, "AppTeveo/pagina_comentario.html", context)


def camara_json(request, id):
    try:
        # Obtener la cámara específica por su ID
        camara = Camara.objects.get(id=id)
        
        # Contar el número de comentarios asociados a esta cámara
        comentarios_count = Comentario.objects.filter(camara=camara).count()
        
        # Construir un diccionario con la información de la cámara y el número de comentarios
        camara_info = {
            'id': camara.id,
            'nombre': camara.nombre,
            'descripcion': camara.descripcion,
            'url': camara.url,
            'lugar': camara.lugar,
            'latitud': camara.latitud,
            'longitud': camara.longitud,
            'numero_comentarios': comentarios_count
        }
        
        # Devolver la información de la cámara en formato JSON
        return JsonResponse(camara_info)
    except Camara.DoesNotExist:
        # Si la cámara no existe, devolver un error 404
        return JsonResponse({'error': 'Cámara no encontrada'}, status=404)


##################### descargar listados ###################

def process_listado(listado):
    try:
        if listado == 'listado1':
            xml_path = os.path.join(settings.BASE_DIR, 'listado1.xml')
            if not os.path.exists(xml_path):
                raise FileNotFoundError(f"El archivo {xml_path} no existe.")
            tree = ET.parse(xml_path)
            root = tree.getroot()

            for camara in root.findall('camara'):
                id_camara_element = camara.find('id')
                if id_camara_element is not None:
                    id_camara = id_camara_element.text
                else:
                    continue

                src_element = camara.find('src')
                if src_element is not None:
                    url = src_element.text
                else:
                    continue

                lugar_element = camara.find('lugar')
                if lugar_element is not None:
                    lugar = lugar_element.text
                else:
                    continue

                coordenadas_element = camara.find('coordenadas')
                if coordenadas_element is not None:
                    coordenadas = coordenadas_element.text
                    latitud_str, longitud_str = coordenadas.split(',')
                    latitud = float(latitud_str)
                    longitud = float(longitud_str)
                else:
                    continue

                # Comprobar si la cámara ya está almacenada en la base de datos
                if not Camara.objects.filter(nombre=id_camara).exists():
                    # Si no existe, crear y guardar la nueva cámara
                    nueva_camara = Camara.objects.create(
                        nombre=id_camara,
                        url=url,
                        lugar=lugar,
                        latitud=latitud,
                        longitud=longitud
                    )
                    nueva_camara.save()

        elif listado == 'listado2':
            xml_path = os.path.join(settings.BASE_DIR, 'listado2.xml')
            if not os.path.exists(xml_path):
                raise FileNotFoundError(f"El archivo {xml_path} no existe.")
            tree = ET.parse(xml_path)
            root = tree.getroot()

            for cam in root.findall('cam'):
                id_camara = cam.get('id')  # Obtenemos el atributo 'id' de la etiqueta 'cam'

                url_element = cam.find('url')
                if url_element is not None:
                    url = url_element.text
                else:
                    continue

                info_element = cam.find('info')
                if info_element is not None:
                    lugar = info_element.text
                else:
                    continue

                place_element = cam.find('place')
                if place_element is not None:
                    latitud_element = place_element.find('latitude')
                    longitud_element = place_element.find('longitude')
                    if latitud_element is not None and longitud_element is not None:
                        latitud = latitud_element.text
                        longitud = longitud_element.text
                    else:
                        continue
                else:
                    continue

                # Comprobar si la cámara ya está almacenada en la base de datos
                if not Camara.objects.filter(nombre=id_camara).exists():
                    # Si no existe, crear y guardar la nueva cámara
                    nueva_camara = Camara.objects.create(
                        nombre=id_camara,
                        url=url,
                        lugar=lugar,
                        latitud=latitud,
                        longitud=longitud
                    )
                    nueva_camara.save()

        if listado == 'listado3':
            kml_url = "http://datos.madrid.es/egob/catalogo/202088-0-trafico-camaras.kml"
            response = requests.get(kml_url)
            
            if response.status_code != 200:
                raise Exception(f"Error al descargar el archivo KML: {response.status_code}")

            xml_content = response.content
            root = ET.fromstring(xml_content)

            # Añadir el espacio de nombres del KML
            ns = {'kml': 'http://earth.google.com/kml/2.2'}

            for placemark in root.findall('.//kml:Placemark', namespaces=ns):
                id_camara_element = placemark.find('.//kml:ExtendedData/kml:Data[@name="Numero"]/kml:Value', namespaces=ns)
                if id_camara_element is not None:
                    id_camara = id_camara_element.text
                else:
                    continue

                description_element = placemark.find('kml:description', namespaces=ns)
                if description_element is not None:
                    url = description_element.text.split('src=')[1].split()[0]
                else:
                    continue

                name_element = placemark.find('.//kml:ExtendedData/kml:Data[@name="Nombre"]/kml:Value', namespaces=ns)
                if name_element is not None:
                    lugar = name_element.text
                else:
                    continue

                coordinates_element = placemark.find('kml:Point/kml:coordinates', namespaces=ns)
                if coordinates_element is not None:
                    coords = coordinates_element.text.strip().split(',')
                    longitud = float(coords[0])
                    latitud = float(coords[1])
                else:
                    continue

                # Comprobar si la cámara ya está almacenada en la base de datos
                if not Camara.objects.filter(nombre=id_camara).exists():
                    # Si no existe, crear y guardar la nueva cámara
                    nueva_camara = Camara.objects.create(
                        nombre=id_camara,
                        url=url,
                        lugar=lugar,
                        latitud=latitud,
                        longitud=longitud
                    )
                    nueva_camara.save()

        elif listado == 'listado4':
            url = 'https://infocar.dgt.es/datex2/dgt/CCTVSiteTablePublication/all/content.xml'
            response = requests.get(url)
            response.raise_for_status()
            tree = ET.ElementTree(ET.fromstring(response.content))
            root = tree.getroot()
            ns = {'_0': 'http://datex2.eu/schema/2/2_0'}

            for record in root.findall('.//_0:cctvCameraMetadataRecord', namespaces=ns):
                id_camara_element = record.find('_0:cctvCameraSerialNumber', namespaces=ns)
                if id_camara_element is not None:
                    id_camara = id_camara_element.text
                else:
                    continue

                url_element = record.find('_0:cctvStillImageService/_0:stillImageUrl/_0:urlLinkAddress', namespaces=ns)
                if url_element is not None:
                    url = url_element.text
                else:
                    continue

                lugar_element = record.find('_0:cctvCameraIdentification', namespaces=ns)
                if lugar_element is not None:
                    lugar = lugar_element.text
                else:
                    continue

                coordinates_element = record.find('_0:cctvCameraLocation/_0:locationForDisplay', namespaces=ns)
                if coordinates_element is not None:
                    latitud_element = coordinates_element.find('_0:latitude', namespaces=ns)
                    longitud_element = coordinates_element.find('_0:longitude', namespaces=ns)
                    if latitud_element is not None and longitud_element is not None:
                        latitud = float(latitud_element.text)
                        longitud = float(longitud_element.text)
                    else:
                        continue
                else:
                    continue

                # Comprobar si la cámara ya está almacenada en la base de datos
                if not Camara.objects.filter(nombre=id_camara).exists():
                    # Si no existe, crear y guardar la nueva cámara
                    nueva_camara = Camara.objects.create(
                        nombre=id_camara,
                        url=url,
                        lugar=lugar,
                        latitud=latitud,
                        longitud=longitud
                    )
                    nueva_camara.save()

    except FileNotFoundError as e:
        # Manejar el error si el archivo no se encuentra
        print(e)
        return HttpResponse(f"Error: {e}")
    except ET.ParseError as e:
        # Manejar el error si el archivo XML no se puede parsear
        print(e)
        return HttpResponse(f"Error al parsear el archivo XML: {e}")
    except Exception as e:
        # Manejar cualquier otro error
        print(e)
        return HttpResponse(f"Se produjo un error inesperado: {e}")

def descargar_fuente_datos(request, listado):
    if request.method == 'POST':
        if listado in ['listado1', 'listado2', 'listado3', 'listado4']:
            response = process_listado(listado)
            if response is not None:
                return response
            return redirect(reverse("camaras"))
        else:
            return HttpResponse("Listado no válido.")
    else:
        return HttpResponse("Esta vista solo admite solicitudes POST.")
