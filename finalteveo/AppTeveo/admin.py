from django.contrib import admin
from .models import Comentario, Camara, Usuario

# Register your models here.
admin.site.register(Comentario)
admin.site.register(Camara)
admin.site.register(Usuario)
