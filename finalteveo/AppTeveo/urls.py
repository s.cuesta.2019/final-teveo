from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('ayuda/', views.pagina_ayuda, name='ayuda'),
    path('configuracion/', views.configuracion, name='configuracion'),
    path('camaras/', views.camaras, name='camaras'),
    path('camaras/<str:id>/', views.camara, name='camara'),
    path('<str:id>-dyn/', views.camara_dinamica, name='camara_dinamica'),
    path('comentario/<int:id>/', views.comentarios, name='comentarios'),
    path('camara/<int:id>/', views.camara_json, name='camara_json'),
    path('teveo/descargar-fuente-datos/<str:listado>/', views.descargar_fuente_datos, name='descargar_fuente_datos'),
    path('camara/<int:id>/imagen', views.imagen_dinamica, name='imagen_dinamica'),
    path('<int:id>/comentarios_dinamicos/', views.comentarios_dinamicos, name='comentarios_dinamicos'),
]