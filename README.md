# ENTREGA CONVOCATORIA Julio

# ENTREGA DE PRÁCTICA

## Datos

* Nombre:Sergio Cuesta Cantudo
* Titulación: Doble Grado en Ingenieria de sistemas de telecomunicaciones y Ade
* Cuenta en laboratorios: sercuest
* Cuenta URJC: s.cuesta.2019@alumnos.urjc.es
* Video básico (url): https://youtu.be/z1b1j7flqt0
* Video parte opcional (url): https://youtu.be/sebMnqb5lEA
* Despliegue (url): https://sercuesta.pythonanywhere.com/
* Contraseñas: sergio
* Cuenta Admin Site: sergio/sergio

## Resumen parte obligatoria

Al poner la url http://127.0.0.1:8000/ te rediriguira directamente a mi pagina principal, que es http://127.0.0.1:8000/teveo. En la pagina te aparecera un menú desde el que se puede acceder, mediante enlaces, a otras páginas, como son a la pagina pincipal, de Cámaras, Configuración, Ayuda y Admin. También aparecerán un listado de comentarios puestos ordenados por camaras, la que tenga mas comentarios primero. Para cada camara se mostrará su id, que sera un enlace a esa camara, la fecha de ese comentario, el comentario y una imagen de la camara.
En la página de cámaras aparecera un listado de las dos fuentes de datos disponibles y una lista con las camaras disponibles. Estas camaras tendran el nombre de la camara, la fecha, el lugar y enlaces para la Página de la Cámara y la Página Dinámica de la Cámara.
En la página de configuración aparece un formulario para elegir el nombre del usuario que registrara el cambio y un formulario para elegir el tipo y el tamaño de la letra.
En la página de ayuda aparece una breve explicación sobre el funcionamiento de la pagina Teveo y un boto para volver a la pagina principal.
En la página de acceso al Admin Site aparece un formulario para iniciar sesión, introducimos el nombre de usuario y la contraseña y nos redirige a la página de administración de Django.
En la página de cada cámara aparece el nombre de la camara, con la informacion de esa camara y debajo una imagen de la camara. Debajo hay tres links, uno que lleva a la pagina de la camara dinamica, otro para poner un comentario y uno para ver la infromacion en json.
En la página json aparece el nombre (id) de la cámara, la descripcion, la url de la camara, la ubicacion de la camara, la latitud y logitud y el numero de comnetarios.


## Lista partes opcionales

* Favicon: He incluido un favicon, que es una logo/icono personal de la pagina para distinguir de otras app TeveO
* Cerrar Sesion: Se habilita un boton para cerra sesion en la pagina de configuracion
* Like Camaras: Se habilita un boton para dar like a las camaras, solo si estas regsitrado
* Se implementan las otras dos fuentes de datos